import React from 'react';
import Hello from 'components/Hello';

export default function App() {
    return <Hello compiler='Typescript' framework='React' />;
}

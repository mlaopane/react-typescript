# Create a React application with Typescript

## Purpose

Start building your **React** application using _types_.

You don't have to worry about `Typescript | Webpack` configuration : it's already pre-configured for you !

NB: A default `<Hello>` component is created and rendered as an example.

## Getting started

### Requirements

You will need :

* `Node 8+`
* `Yarn 1.7+`

### Installation

```shell
yarn install
```

### Development

```shell
yarn dev
```

Then start coding (^\_^)

### Deployment

```shell
yarn build
```

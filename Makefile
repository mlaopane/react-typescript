clean: uninstall
init: install
start: server_start

install:
	yarn install

uninstall:
	rm -rf node_modules

server_start:
	yarn start
